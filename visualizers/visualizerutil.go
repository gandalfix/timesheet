package visualizers

import (
	"fmt"
	"math"
	"time"
)

// TranslateWeekday translates the given weekday into a german string (abbreviation of weekday)
func TranslateWeekday(weekday time.Weekday) string {
	switch weekday {
	case time.Monday:
		return "Mo"
	case time.Tuesday:
		return "Di"
	case time.Wednesday:
		return "Mi"
	case time.Thursday:
		return "Do"
	case time.Friday:
		return "Fr"
	case time.Saturday:
		return "Sa"
	case time.Sunday:
		return "So"
	}
	return ""
}

// FormatDuration formats a duration in hh:mm format
func FormatDuration(dur time.Duration) string {
	hours := int(dur.Hours())
	//logging.Error.Printf("%v ---- %v", hours, dur.Minutes())
	minutes := math.Abs(dur.Minutes() - float64(hours*60))
	return fmt.Sprintf("%02d:%02.f", hours, minutes)
}
