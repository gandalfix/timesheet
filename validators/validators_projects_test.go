package validators_test

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
	"bitbucket.org/gandalfix/timesheet/validators"
)

func init() {
	logging.InitLog(ioutil.Discard, ioutil.Discard, os.Stderr, os.Stderr)
}

func TestValidateProjectOk(t *testing.T) {
	timesheet := types.MakeTimesheet(types.MakeSampleProjectDefinitions(), types.MakeSampleDays())
	ok, errors := validators.ValidateProjects(*timesheet)
	if !ok {
		t.Error("Expected to be valid timesheet: " + errors[0].Error())
	}
}

func TestValidateProjectFail(t *testing.T) {
	days := make(map[time.Time]*types.DayEntry)

	time, _ := time.Parse("2006-01-02", "2016-01-01")

	days[time] = types.MakeDayEntry(time, []types.TimeEntry{
		*types.MakeTimeEntry("2016-01", "18:00", "19:00", "PRJ5", ""),
	})

	timesheet := types.MakeTimesheet(types.MakeSampleProjectDefinitions(), days)
	ok, errors := validators.ValidateProjects(*timesheet)
	if ok {
		t.Error("Expected to be invalid timesheet: " + errors[0].Error())
	}
}
