package parser_test

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/parser"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	logging.InitLog(ioutil.Discard, ioutil.Discard, os.Stderr, os.Stderr)
}

func TestParser(t *testing.T) {

	settings := &types.TimesheetSettings{
		Year:     2015,
		Filename: "../test_timesheet.times",
	}

	p := &parser.Parser{
		Settings: *settings,
	}
	success, error := p.Parse()

	if !success {
		t.Error("Parsing failed: ", error)
	} else {
		if len(p.Timesheet.Projects) == 11 {
			verifyParsedProjects(p.Timesheet, t)
		} else {
			t.Error("Expected 11 projects to be identified, but got", len(p.Timesheet.Projects))
		}
		if len(p.Timesheet.DayEntries) == 5 {
			verifyParsedDays(p.Timesheet, t)
		} else {
			t.Error("Expected 5 days to be identified, but got", len(p.Timesheet.DayEntries))
		}
	}
}

func verifyParsedDays(timesheet types.Timesheet, t *testing.T) {
	verifyDay(timesheet, "2015-04-24", t)
	verifyDay(timesheet, "2015-04-27", t)
	verifyDay(timesheet, "2015-04-28", t)
	verifyDay(timesheet, "2015-04-29", t)
	verifyDay(timesheet, "2015-04-30", t)
}

func verifyDay(timesheet types.Timesheet, date string, t *testing.T) {
	day, _ := time.Parse("2006-01-02", date)
	dayEntryFromTimesheet, ok := timesheet.DayEntries[day]

	if !ok {
		t.Error("unable to find day entry for", date)
	}

	numberOfTimeEntries := len(dayEntryFromTimesheet.Times)

	switch date {
	case "2015-04-24":
		addErrorIfTimeEntriesDoNotMatch(1, numberOfTimeEntries, t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[0], "08:00", "16:00", "GanzFrei", "", t)
	case "2015-04-27":
		addErrorIfTimeEntriesDoNotMatch(5, numberOfTimeEntries, t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[0], "08:00", "10:20", "PRJ1", "Interne Aufgaben erledigt", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[1], "10:40", "10:50", "PRJ2", "Fahrt Haus - Kunde", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[2], "10:50", "12:00", "KundenProj", "Ganz normale Projektarbeit", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[3], "12:00", "13:00", "PAUSE", "Mittagessen mit netten Kollegen", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[4], "13:00", "17:00", "KundenProj2", "", t)
	case "2015-04-28":
		addErrorIfTimeEntriesDoNotMatch(1, numberOfTimeEntries, t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[0], "08:00", "16:00", "Urlaub", "Freizeit muss auch mal sein", t)
	case "2015-04-29":
		addErrorIfTimeEntriesDoNotMatch(1, numberOfTimeEntries, t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[0], "09:00", "17:00", "Krank", "", t)
	case "2015-04-30":
		addErrorIfTimeEntriesDoNotMatch(3, numberOfTimeEntries, t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[0], "08:00", "10:45", "PRJ1", "Officearbeit", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[1], "10:45", "10:55", "PRJ2", "Fahrtzeit zum Kunden", t)
		addErrorIfTimeEntryIsWrong(dayEntryFromTimesheet.Times[2], "10:55", "18:00", "KundenProj", "Ohne Pause durchgearbeitet", t)
	}
}

func addErrorIfTimeEntryIsWrong(actual types.TimeEntry, start string, end string, project string, comment string, t *testing.T) {
	if actual.StartTime.Format("15:04") != start {
		t.Errorf("Expected %s got %s for start time", start, actual.StartTime.Format("15:04"))
	}
	if actual.EndTime.Format("15:04") != end {
		t.Errorf("Expected %s got %s for end time", end, actual.EndTime.Format("15:04"))
	}
	if actual.ProjectCode != project {
		t.Errorf("Expected %s got %s for projectCode", project, actual.ProjectCode)
	}
	if actual.Comment != comment {
		t.Errorf("Expected %s got %s for comment", comment, actual.Comment)
	}
}

func addErrorIfTimeEntriesDoNotMatch(expectedNumber int, actualValues int, t *testing.T) {
	if expectedNumber != actualValues {
		t.Errorf("Expected %d got %d time entries", expectedNumber, actualValues)
	}
}

func verifyParsedProjects(timesheet types.Timesheet, t *testing.T) {
	verifyProject(timesheet.Projects["PRJ1"], "PRJ1", "PRJ-INTERN", "OfficeTime", "", t)
	verifyProject(timesheet.Projects["PRJ2"], "PRJ2", "PRJ-INTERN", "Fahrtzeit", "", t)
	verifyProject(timesheet.Projects["KundenProj"], "KundenProj", "P99105457-002", "", "", t)
	verifyProject(timesheet.Projects["KundenProj2"], "KundenProj2", "PAA1657-003", "Working", "--hideDescription", t)
	verifyProject(timesheet.Projects["KundenProj3"], "KundenProj3", "P99105457-002", "V. Name", "", t)
	verifyProject(timesheet.Projects["PAUSE"], "PAUSE", "XXX", "", "--ignoreProject", t)
	verifyProject(timesheet.Projects["Unterstunden"], "Unterstunden", "Timeoff", "Unterstunden_(optional)", "", t)
	verifyProject(timesheet.Projects["GanzFrei"], "GanzFrei", "Timeoff", "Unterstundentag", "", t)
	verifyProject(timesheet.Projects["Lunch"], "Lunch", "P001000-001", "XX", "--ignoreProject", t)
	verifyProject(timesheet.Projects["Urlaub"], "Urlaub", "Timeoff", "Urlaub", "", t)
	verifyProject(timesheet.Projects["Krank"], "Krank", "Timeoff", "Krank", "", t)
}

func verifyProject(project types.ProjectDefinition, expProjectCode string, expProjectName string, expProjectComment string, expProjectOption string, t *testing.T) {
	if project.ProjectCode != expProjectCode {
		t.Errorf("Expected projectCode '%s' got '%s'", expProjectCode, project.ProjectCode)
	}
	if project.ProjectName != expProjectName {
		t.Errorf("Expected projectName '%s' got '%s'", expProjectName, project.ProjectName)
	}
	if project.ProjectComment != expProjectComment {
		t.Errorf("Expected projectComment '%s' got '%s'", expProjectComment, project.ProjectComment)
	}
	if project.ProjectOption != expProjectOption {
		t.Errorf("Expected projectOption '%s' got '%s'", expProjectOption, project.ProjectOption)
	}
}
