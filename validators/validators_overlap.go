package validators

import (
	"errors"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterValidator(&TimesheetValidator{
		Name:        "Overlap",
		Description: "Validates whether there are overlaps in times within a single day",
		Fatal:       false,
		Validate:    ValidateOverlap,
		Priority:    9010,
	})
}

// ValidateOverlap validates whether there is overlap
func ValidateOverlap(timesheet types.Timesheet) (ok bool, validationErrors []error) {
	for day, entry := range timesheet.DayEntries {
		logging.Info.Printf("%s", day.Format("02.01.2006"))
		valid, validationErrors := validateOverlapDay(entry)
		if !valid {
			return false, validationErrors
		}
	}
	return true, nil
}

func validateOverlapDay(dayEntry *types.DayEntry) (ok bool, validationErrors []error) {
	for index, timeEntry := range dayEntry.Times {
		logging.Trace.Printf("Entry  : %s - %s", timeEntry.StartTime.Format("2006-01-02 15:04"), timeEntry.EndTime.Format("15:04"))
		subEntries := dayEntry.Times[index+1:]
		//logging.Error.Println(subEntries)
		for _, toCompare := range subEntries {
			logging.Trace.Printf("Compare: %s - %s", toCompare.StartTime.Format("2006-01-02 15:04"), toCompare.EndTime.Format("15:04"))

			// toCompare starts between start and end of timeEntry
			startBetween := !timeEntry.StartTime.After(toCompare.StartTime) && toCompare.StartTime.Before(timeEntry.EndTime)
			// toCompare starts before start of timeEntry and ends afterwards
			startBeforeEndAfter := timeEntry.StartTime.After(toCompare.StartTime) && toCompare.EndTime.After(timeEntry.StartTime)

			logging.Trace.Println("Overlap debugging: StartBetween", startBetween, "StartBeforeEndAfter", startBeforeEndAfter)

			if (startBetween) || (startBeforeEndAfter) {
				message := "Overlapping entries on " + dayEntry.Day.Format("02.01.2006") + " starting : " + timeEntry.StartTime.Format("15:04")
				return false, []error{errors.New(message)}
			}
		}
	}
	return true, []error{}
}
