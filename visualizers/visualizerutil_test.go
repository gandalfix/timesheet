package visualizers_test

import (
	"testing"
	"time"

	"bitbucket.org/gandalfix/timesheet/visualizers"
)

var durFormat = []struct {
	duration time.Duration
	format   string
}{
	{time.Hour*3 + time.Minute*0, "03:00"},
	{time.Hour*3 + time.Minute*3, "03:03"},
	{time.Hour*0 + time.Minute*0, "00:00"},
	{time.Hour*12 + time.Minute*13, "12:13"},
	{time.Hour*1 + time.Minute*61, "02:01"},
	{time.Hour*99 + time.Minute*1, "99:01"},
}

func TestFormatDuration(t *testing.T) {
	for _, dat := range durFormat {
		if visualizers.FormatDuration(dat.duration) != dat.format {
			t.Errorf("Formatting not correct for '%s'", dat.format)
		}
	}
}

var weekdayFormat = []struct {
	day    time.Weekday
	format string
}{
	{time.Monday, "Mo"},
	{time.Tuesday, "Di"},
	{time.Wednesday, "Mi"},
	{time.Thursday, "Do"},
	{time.Friday, "Fr"},
	{time.Saturday, "Sa"},
	{time.Sunday, "So"},
}

func TestTranslateWeekday(t *testing.T) {
	for _, dat := range weekdayFormat {
		if visualizers.TranslateWeekday(dat.day) != dat.format {
			t.Errorf("Formatting not correct for '%s'", dat.format)
		}
	}
}
