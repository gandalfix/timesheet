package visualizers

import (
	"fmt"
	"math"
	"strings"
	"time"

	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterVisualizer(&TimesheetVisualizer{
		Name:        "Monthly",
		Description: "Prints out a monthly overview",
		Visualize:   VisualizeMonthly,
	})
}

func buildHeader(projects []string, days []time.Time) string {
	header := ""
	header = fmt.Sprintf("      ")
	for _, code := range projects {
		header += fmt.Sprintf(" | %5s", code)
	}
	header += fmt.Sprintf(" || Summe")

	return header
}

func buildSeparator(header string) string {
	return strings.Repeat("-", len(header))
}

func buildDayLine(day time.Time, projects []string, dayMapPerProject map[string]time.Duration) string {
	var dayLine string
	var dailySum time.Duration

	dayLine += fmt.Sprintf("%s", day.Format("02.01."))

	for _, code := range projects {
		dur := dayMapPerProject[code]
		dailySum += dur
		formatString := fmt.Sprintf(" | %%%vs", len(code))
		dayLine += fmt.Sprintf(formatString, FormatDuration(dur))
	}
	dayLine += fmt.Sprintf(" || %s", FormatDuration(dailySum))

	return dayLine
}

func buildSumLine(projects []string, monthMapPerProject map[string]time.Duration) string {
	var sumLine string
	sumLine += fmt.Sprintf("Summe ")
	var overallSum time.Duration
	for _, code := range projects {
		dur := monthMapPerProject[code]
		overallSum += dur
		formatString := fmt.Sprintf(" | %%%vs", math.Max(float64(len(code)), 6))
		sumLine += fmt.Sprintf(formatString, FormatDuration(dur))
	}
	sumLine += fmt.Sprintf(" || %s", FormatDuration(overallSum))
	return sumLine
}

func buildSumDayLine(projects []string, monthMapPerProject map[string]time.Duration) string {
	var sumLine string
	sumLine += fmt.Sprintf("Tage  ")
	var overallSum float64
	for _, code := range projects {
		dur := monthMapPerProject[code]
		overallSum += dur.Hours() / 8
		formatString := fmt.Sprintf(" | %%%v.1f ", math.Max(float64(len(code))-1, 4))
		sumLine += fmt.Sprintf(formatString, (dur.Hours() / 8))
	}
	sumLine += fmt.Sprintf(" || %4.1f", overallSum)
	return sumLine
}

// VisualizeMonthly prints a monthly overview of the timesheet
func VisualizeMonthly(timesheet types.Timesheet) {
	codes := timesheet.GetUsedProjectCodesSorted(true)
	days := timesheet.GetDaysSorted()

	header := buildHeader(codes, days)
	fmt.Println(header)
	fmt.Println(buildSeparator(header))

	monthMapPerProject := make(map[string]time.Duration)

	for _, day := range days {

		dayMapPerProject := make(map[string]time.Duration)
		de := timesheet.DayEntries[day]
		for _, te := range de.Times {
			dur := te.EndTime.Sub(te.StartTime)
			dayMapPerProject[te.ProjectCode] = dayMapPerProject[te.ProjectCode] + dur
			monthMapPerProject[te.ProjectCode] = monthMapPerProject[te.ProjectCode] + dur
		}

		dayLine := buildDayLine(day, codes, dayMapPerProject)

		fmt.Println(dayLine)
	}

	fmt.Println(buildSeparator(header))

	sumLine := buildSumLine(codes, monthMapPerProject)
	fmt.Println(sumLine)
	sumDayLine := buildSumDayLine(codes, monthMapPerProject)
	fmt.Println(sumDayLine)
}
