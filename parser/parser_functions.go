package parser

import (
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func handleProjectDefinition(line string, timesheet *types.Timesheet) (success bool, error error) {

	matches := projectDefinitionRegexp.FindStringSubmatch(line)

	projectDefinition := &types.ProjectDefinition{
		ProjectCode: strings.TrimSpace(matches[1]),
		ProjectName: strings.TrimSpace(matches[2]),
	}

	fillProjectOptionIfApplicable(strings.TrimSpace(matches[4]), projectDefinition)

	logging.Trace.Printf("ProjectCode: %s, ProjectName: %s, ProjectComment: %s, ProjectOption: %s\n",
		projectDefinition.ProjectCode,
		projectDefinition.ProjectName,
		projectDefinition.ProjectComment,
		projectDefinition.ProjectOption)

	timesheet.Projects[projectDefinition.ProjectCode] = *projectDefinition

	return true, nil
}

func fillProjectOptionIfApplicable(possibleOption string, projectDefinition *types.ProjectDefinition) {
	if strings.HasSuffix(possibleOption, "--hideDescription") {
		projectDefinition.ProjectComment = strings.TrimSpace(strings.TrimSuffix(possibleOption, "--hideDescription"))
		projectDefinition.ProjectOption = "--hideDescription"
	} else if strings.HasSuffix(possibleOption, "--ignoreProject") {
		projectDefinition.ProjectComment = strings.TrimSpace(strings.TrimSuffix(possibleOption, "--ignoreProject"))
		projectDefinition.ProjectOption = "--ignoreProject"
	} else {
		projectDefinition.ProjectComment = strings.TrimSpace(possibleOption)
	}

}

func identifyLineType(line string) lineType {
	if strings.TrimSpace(line) == "" {
		return emptyLine
	}
	if lineCommentRegexp.MatchString(line) {
		return lineCommentLine
	}
	if projectDefinitionRegexp.MatchString(line) {
		return projectDefinitionLine
	}
	if dayRegexp.MatchString(line) {
		return dayDefinitionLine
	}
	if dayEntryRegExp.MatchString(line) {
		return timeEntryLine
	}
	return unknownLine
}

func handleDayEntryDefinition(line string, dayEntry *types.DayEntry, year int) (success bool, error error) {
	matches := dayRegexp.FindStringSubmatch(line)
	weekDay := matches[1]
	date := matches[2]
	date += strconv.Itoa(year)

	logging.Trace.Printf("Converting weekday '%s' and date '%s'", weekDay, date)

	parsedDay, parseError := time.Parse("2.1.2006", date)

	if parseError != nil {
		return false, parseError
	}
	success, weekdayError := matchesWeekDay(weekDay, parsedDay)
	if !success {
		return success, weekdayError
	}
	dayEntry.Day = parsedDay

	return true, nil
}

func matchesWeekDay(stringWeekday string, day time.Time) (success bool, error error) {
	var expectedWeekday time.Weekday
	switch stringWeekday {
	case "Mo":
		expectedWeekday = time.Monday
	case "Di":
		expectedWeekday = time.Tuesday
	case "Mi":
		expectedWeekday = time.Wednesday
	case "Do":
		expectedWeekday = time.Thursday
	case "Fr":
		expectedWeekday = time.Friday
	case "Sa":
		expectedWeekday = time.Saturday
	case "So":
		expectedWeekday = time.Sunday
	}
	if expectedWeekday != day.Weekday() {
		return false, errors.New("For " + strconv.Itoa(day.Day()) + " the weekday is incorrect. Should be " + day.Weekday().String())
	}
	return true, nil
}

func handleTimeEntryDefinition(line string, currentDay *types.DayEntry) (success bool, error error) {
	matches := dayEntryRegExp.FindStringSubmatch(line)

	logging.Trace.Printf("Read time entry: Start %s, End: %s, Project: %s, Comment: '%s'", matches[1], matches[2], matches[3], matches[4])

	startDate := createTime(currentDay.Day, matches[1])
	endDate := createTime(currentDay.Day, matches[2])
	projectCode := strings.TrimSpace(matches[3])
	comment := strings.TrimSpace(matches[4])

	if !startDate.Before(endDate) {
		return false, errors.New("Start time is not before endDate: " + matches[1] + " - " + matches[2])
	}

	timeEntry := &types.TimeEntry{
		StartTime:   startDate,
		EndTime:     endDate,
		ProjectCode: projectCode,
		Comment:     comment,
	}

	logging.Trace.Printf("Parsed time entry: Start %s, End: %s, Project: %s, Comment: '%s'", timeEntry.StartTime, timeEntry.EndTime, timeEntry.ProjectCode, timeEntry.Comment)

	currentDay.Times = append(currentDay.Times, *timeEntry)

	sort.Sort(types.ByTimeEntry(currentDay.Times))

	return true, nil
}

func createTime(day time.Time, timeString string) time.Time {
	dayAsString := day.Format("2006-01-02")
	dayWithTime := dayAsString + " " + timeString
	parsedDay, _ := time.Parse("2006-01-02 15:04", dayWithTime)
	return parsedDay
}
