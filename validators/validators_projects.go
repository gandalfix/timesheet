package validators

import (
	"errors"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterValidator(&TimesheetValidator{
		Name:        "Projects",
		Description: "Validates whether all projects used are defined within the file",
		Fatal:       true,
		Validate:    ValidateProjects,
		Priority:    9900,
	})
}

// ValidateProjects validates whether all projects used are defined within the file
func ValidateProjects(timesheet types.Timesheet) (ok bool, validationErrors []error) {
	for day, entry := range timesheet.DayEntries {
		logging.Info.Printf("%s", day.Format("02.01.2006"))
		for _, timeEntry := range entry.Times {
			if timeEntry.ProjectCode == "Urlaub" || timeEntry.ProjectCode == "Krank" {
				logging.Trace.Println("Identified Urlaub or Krank on " + day.Format("02.01.2006"))
				continue
			}
			_, projectFound := timesheet.Projects[timeEntry.ProjectCode]
			if !projectFound {
				return false, []error{errors.New("Unknown project code '" + timeEntry.ProjectCode + "' found")}
			}
		}
	}
	return true, []error{}
}
