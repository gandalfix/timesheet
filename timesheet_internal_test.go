package main

import "testing"

func TestFilenameRegexp(t *testing.T) {
	lines := []string{
		"2015_jan.times",
		"2010_Feb.times",
		"2999_mAr.times",
		"2016_apR.times",
		"2011_MAy.times",
		"1999_JuN.times",
		"1234_jUL.times",
		"2015_AUG.times",
		"2015_Sep.times",
		"2015_Oct.times",
		"2015_NOV.times",
		"9999_DEC.times",
	}

	for _, line := range lines {
		if !fileNameRegExp.MatchString(line) {
			t.Error("'", line, "' should be a valid filename but does not match")
		}
	}
}
