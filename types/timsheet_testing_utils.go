package types

import "time"

// MakeTimesheet creates a timesheet instance
func MakeTimesheet(projectDefinitions map[string]ProjectDefinition, dayEntries map[time.Time]*DayEntry) *Timesheet {
	return &Timesheet{
		Projects:   projectDefinitions,
		DayEntries: dayEntries,
	}
}

// MakeSampleProjectDefinitions creates a collection of projectDefinitions
func MakeSampleProjectDefinitions() map[string]ProjectDefinition {
	defs := make(map[string]ProjectDefinition)

	defs["PRJ1"] = *MakeProjectDefinition("PRJ1", "PROJECT1", "COMMENT1", "")
	defs["PRJ2"] = *MakeProjectDefinition("PRJ2", "PROJECT1", "", "")
	defs["PRJ3"] = *MakeProjectDefinition("PRJ3", "PROJECT3", "", "--ignoreProject")
	defs["PRJ4"] = *MakeProjectDefinition("PRJ4", "PROJECT4", "Working", "--hideDescription")

	return defs
}

// MakeSampleDays creates a collection of day entries
func MakeSampleDays() map[time.Time]*DayEntry {
	defs := make(map[time.Time]*DayEntry)

	day, _ := time.Parse("2006-01-02", "2016-01-02")

	defs[day] = MakeDayEntry(day, []TimeEntry{
		*MakeTimeEntry("2016-02", "08:00", "16:00", "PRJ1", "Line Comment"),
		*MakeTimeEntry("2016-02", "17:00", "18:00", "PRJ2", "Line Comment"),
		*MakeTimeEntry("2016-02", "18:00", "19:00", "PRJ3", ""),
	})

	day, _ = time.Parse("2006-01-02", "2016-01-01")

	defs[day] = MakeDayEntry(day, []TimeEntry{
		*MakeTimeEntry("2016-01", "08:00", "16:00", "PRJ1", "Line Comment"),
		*MakeTimeEntry("2016-01", "17:00", "18:00", "PRJ2", "Line Comment"),
		*MakeTimeEntry("2016-01", "18:00", "19:00", "PRJ3", ""),
	})

	return defs
}

// MakeProjectDefinition creates a ProjectDefinition instance
func MakeProjectDefinition(projectCode string, projectName string, projectComment string, projectOption string) *ProjectDefinition {
	return &ProjectDefinition{
		ProjectCode:    projectCode,
		ProjectName:    projectName,
		ProjectComment: projectComment,
		ProjectOption:  projectOption,
	}
}

// MakeDayEntry creates a day entry instance
func MakeDayEntry(day time.Time, timeEntries []TimeEntry) *DayEntry {
	dayEntry := &DayEntry{
		Day:   day,
		Times: timeEntries,
	}
	return dayEntry
}

// MakeTimeEntry creates a TimeEntry instance
func MakeTimeEntry(day string, startTime string, endTime string, project string, comment string) *TimeEntry {
	entry := &TimeEntry{
		ProjectCode: project,
		Comment:     comment,
	}
	start, _ := time.Parse("2006-01-02 15:04", day+" "+startTime)
	end, _ := time.Parse("2006-01-02 15:04", day+" "+endTime)
	entry.StartTime = start
	entry.EndTime = end
	return entry
}
