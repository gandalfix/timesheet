package validators

import (
	"errors"
	"fmt"
	"math"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterValidator(&TimesheetValidator{
		Name:        "Granularity",
		Description: "Validates whether all time entries within the file match the required granularity. Default 3 min (0.1 hour)",
		Fatal:       false,
		Validate:    ValidateGranularity,
		Priority:    9000,
	})
	types.App.Cli.Flag("granularity", "Sets the granularity of the time entry validation.\nIf set to zero, this check is disabled").Default("0.05").Float64Var(&granularity)
}

var (
	granularity = 0.05
)

// ValidateGranularity validates whether all time entries match the required granularity
func ValidateGranularity(timesheet types.Timesheet) (ok bool, validationErrors []error) {
	if granularity == 0.0 {
		return true, nil
	}

	for day, entry := range timesheet.DayEntries {
		logging.Info.Printf("%s", day.Format("02.01.2006"))
		for _, timeEntry := range entry.Times {
			dur := timeEntry.EndTime.Sub(timeEntry.StartTime)
			if !hasCorrectGranularity(dur, granularity) {
				message := fmt.Sprintf("On %s from %s to %s does not comply to granularity %v", day.Format("02.01.2006"), timeEntry.StartTime.Format("15:04"), timeEntry.EndTime.Format("15:04"), granularity)
				return false, []error{
					errors.New(message),
				}
			}
		}
	}
	return true, []error{}
}

func hasCorrectGranularity(duration time.Duration, granularity float64) bool {
	granularityScaled := granularity * 100
	durFixed := toFixed(duration.Hours(), 4) * 100
	mod := toFixed(math.Mod(durFixed, granularityScaled), 4)
	return mod == 0
}

func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}
