package visualizers

import (
	"fmt"

	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterVisualizer(&TimesheetVisualizer{
		Name:        "Normalize",
		Description: "Prints out the timesheet normalized",
		Visualize:   VisualizeNormalize,
	})
}

// VisualizeNormalize normalizes the timesheet and prints it with a standardized format that should be parsable by the tool again
func VisualizeNormalize(timesheet types.Timesheet) {
	codes := timesheet.GetProjectCodesSorted()
	for _, code := range codes {
		project := timesheet.Projects[code]
		fmt.Printf("%s %s %s %s\n", project.ProjectCode, project.ProjectName, project.ProjectComment, project.ProjectOption)
	}
	fmt.Println("")
	days := timesheet.GetDaysSorted()
	for _, day := range days {
		fmt.Printf("* %s. %s\n", TranslateWeekday(day.Weekday()), day.Format("02.01."))
		// not sorting timeentries here, as we are assuming that the parser already sorts them
		for _, timeEntry := range timesheet.DayEntries[day].Times {
			fmt.Printf("%s - %s %s %s\n", timeEntry.StartTime.Format("15:04"), timeEntry.EndTime.Format("15:04"), timeEntry.ProjectCode, timeEntry.Comment)
		}
		fmt.Println("")
	}
}
