package validators

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	logging.InitLog(ioutil.Discard, ioutil.Discard, os.Stderr, os.Stderr)
}

func TestValidateOverlapDayFail(t *testing.T) {
	days := []*types.DayEntry{
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "09:00", "10:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "09:00", "17:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "17:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "16:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "08:00", "17:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "08:00", "17:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "17:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "16:00", "PRJ1", "comment string")},
		},
	}

	for _, day := range days {
		valid, _ := validateOverlapDay(day)
		if valid {
			t.Errorf("Expected error as time entries overlap, on Day: " + day.Day.Format("2006-01-02"))
		}
	}
}

func TestValidateOverlapDayOk(t *testing.T) {
	days := []*types.DayEntry{
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "16:00", "17:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "08:00", "PRJ1", "comment string")},
		},
		&types.DayEntry{
			Day: time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local),
			Times: []types.TimeEntry{
				*types.MakeTimeEntry("2016-01-01", "08:00", "16:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "07:00", "08:00", "PRJ1", "comment string"),
				*types.MakeTimeEntry("2016-01-01", "16:00", "17:00", "PRJ1", "comment string"),
			},
		},
	}

	for _, day := range days {
		valid, _ := validateOverlapDay(day)
		if !valid {
			t.Errorf("Expected to be ok as time entries do not overlap, on Day: " + day.Day.Format("2006-01-02"))
		}
	}
}
