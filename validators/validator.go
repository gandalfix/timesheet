package validators

import (
	"sort"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

// Validator for timesheets
type Validator struct {
	Timesheet types.Timesheet
	Settings  types.TimesheetSettings
}

// TimesheetValidator validates a whole timesheet
type TimesheetValidator struct {
	// Name of this timesheet validator
	Name string
	// Description of this validator
	Description string
	// Fatal indicates whether the validator forces the whole validation process to abort or not
	Fatal bool
	// Priority of this validator. Higher number indicates more important. 0 is default.
	// Built in validators have normally 9000 and above as priority
	Priority int
	// Validate the provided timesheet
	Validate func(timesheet types.Timesheet) (ok bool, errors []error)
}

var validatorRegistry []*TimesheetValidator

// ByPriority sorting struct for TimesheetValidators by Priority
type ByPriority []*TimesheetValidator

func (x ByPriority) Len() int      { return len(x) }
func (x ByPriority) Swap(i, j int) { x[i], x[j] = x[j], x[i] }
func (x ByPriority) Less(i, j int) bool {
	return x[i].Priority > x[j].Priority
}

// Validate the timesheet with all registere TimesheetValidators
func (validator *Validator) Validate() (success bool, errors []error) {
	valid := true

	sort.Sort(ByPriority(validatorRegistry))

	for _, v := range validatorRegistry {
		logging.Info.Printf("Executing validator '%s'", v.Name)
		ok, validatorErrors := v.Validate(validator.Timesheet)
		valid = valid && ok

		if len(validatorErrors) != 0 {
			errors = append(errors, validatorErrors...)
		}
		if !ok {
			if v.Fatal {
				return valid, errors
			}
		}
	}
	return valid, errors
}

// RegisterValidator registers the provided validator with the registry for validation
func RegisterValidator(validator *TimesheetValidator) {
	validatorRegistry = append(validatorRegistry, validator)
}
