package types_test

import (
	"testing"

	"bitbucket.org/gandalfix/timesheet/types"
)

func TestGetProjectCodesSorted(t *testing.T) {
	timesheet := types.MakeTimesheet(nil, types.MakeSampleDays())
	days := timesheet.GetDaysSorted()
	if days[0].Format("2006-01-02") != "2016-01-01" {
		t.Error("First element not 2016-01-01")
	}
	if days[1].Format("2006-01-02") != "2016-01-02" {
		t.Error("Second element not 2016-01-02")
	}
}
