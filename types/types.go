package types

import (
	"sort"
	"strings"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	// App is the main information holder for the application
	App = &TimesheetApp{
		Cli:      kingpin.New("Timesheet parser", "ASCII-Timesheet parser application"),
		Settings: &TimesheetSettings{},
	}
)

// TimesheetApp holds all relevant information for the application
type TimesheetApp struct {
	Cli             *kingpin.Application
	ParsedTimesheet *Timesheet
	Settings        *TimesheetSettings
}

// TimesheetSettings settings for the application
type TimesheetSettings struct {
	Filename       string
	Verbose        bool
	Year           int
	VisualizerName string
}

// ProjectDefinition defines a project
type ProjectDefinition struct {
	ProjectCode    string
	ProjectName    string
	ProjectComment string
	ProjectOption  string
}

// TimeEntry represents a entry with start and end time for some
// actual deed
type TimeEntry struct {
	StartTime   time.Time
	EndTime     time.Time
	ProjectCode string
	Comment     string
}

// DayEntry represents a day consisting TimeEntries
type DayEntry struct {
	Day   time.Time
	Times []TimeEntry
}

// Timesheet is the representation of a file
type Timesheet struct {
	Projects        map[string]ProjectDefinition
	DefaultProjects map[string]ProjectDefinition
	DayEntries      map[time.Time]*DayEntry
}

// NewTimesheet creates a new Timesheet instance with the maps initialized
func NewTimesheet() *Timesheet {
	//[]string{"Krank", "Urlaub"},
	timesheet := &Timesheet{
		Projects:   make(map[string]ProjectDefinition),
		DayEntries: make(map[time.Time]*DayEntry),
	}

	timesheet.Projects["Urlaub"] = *&ProjectDefinition{
		ProjectCode:    "Urlaub",
		ProjectName:    "Timeoff",
		ProjectComment: "Urlaub",
	}

	timesheet.Projects["Krank"] = *&ProjectDefinition{
		ProjectCode:    "Krank",
		ProjectName:    "Timeoff",
		ProjectComment: "Krank",
	}

	return timesheet
}

// GetProjectCodesSorted returns the available project codes alphabetical sorted.
func (timesheet *Timesheet) GetProjectCodesSorted() []string {
	requiredLen := len(timesheet.Projects)
	codes := make([]string, requiredLen)
	i := 0
	for code := range timesheet.Projects {
		codes[i] = code
		i++
	}
	sort.Strings(codes)
	return codes
}

// GetUsedProjectCodesSorted returns only the project codes that are actually used.
// If filterIgnoredProjects is set to true, projects with comment "--ignoreProject" are not returned
func (timesheet *Timesheet) GetUsedProjectCodesSorted(filterIgnoredProjects bool) []string {
	usedCodes := make(map[string]string)
	i := 0
	for _, dayEntry := range timesheet.DayEntries {
		for _, timeEntry := range dayEntry.Times {
			project := timesheet.Projects[timeEntry.ProjectCode]

			if filterIgnoredProjects && strings.Contains(project.ProjectOption, "--ignoreProject") {
				continue
			}
			usedCodes[timeEntry.ProjectCode] = timeEntry.ProjectCode
		}
	}

	codes := make([]string, len(usedCodes))
	for code := range usedCodes {
		codes[i] = code
		i++
	}
	sort.Strings(codes)
	codes = append(codes)
	return codes
}

// GetDaysSorted returns the days within the timesheet sorted ascending
func (timesheet *Timesheet) GetDaysSorted() []time.Time {
	days := make([]time.Time, len(timesheet.DayEntries))
	i := 0
	for day := range timesheet.DayEntries {
		days[i] = day
		i++
	}
	sort.Sort(ByTime(days))
	return days
}

// ByTime sorting struct for time slices
type ByTime []time.Time

func (x ByTime) Len() int      { return len(x) }
func (x ByTime) Swap(i, j int) { x[i], x[j] = x[j], x[i] }
func (x ByTime) Less(i, j int) bool {
	return x[i].Before(x[j])
}

// ByTimeEntry sorting struct for TimeEntry slices
type ByTimeEntry []TimeEntry

func (x ByTimeEntry) Len() int      { return len(x) }
func (x ByTimeEntry) Swap(i, j int) { x[i], x[j] = x[j], x[i] }
func (x ByTimeEntry) Less(i, j int) bool {
	return x[i].StartTime.Before(x[j].StartTime)
}
