package main

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"gopkg.in/alecthomas/kingpin.v2"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/parser"
	"bitbucket.org/gandalfix/timesheet/types"
	"bitbucket.org/gandalfix/timesheet/validators"
	"bitbucket.org/gandalfix/timesheet/visualizers"
)

var fileNameRegExp = regexp.MustCompile(`(?i)^(\d{4})_(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\.times$`)

func configureCommandLineParameters() {
	types.App.Cli.Flag("file", "Name of the file that should be parsed").Short('f').Required().ExistingFileVar(&types.App.Settings.Filename)
	types.App.Cli.Flag("verbose", "Increased log level").BoolVar(&types.App.Settings.Verbose)
	types.App.Cli.Flag("year", "Year for which the file is. If not present it is assumed that the filename has the format YYYY_MMM.times").Short('y').IntVar(&types.App.Settings.Year)

	types.App.Cli.Flag("Visualizer", "Name of the visualizer that should be used").Short('v').Default("Monthly").EnumVar(&types.App.Settings.VisualizerName, visualizers.GetVisualizerNames()...)
}

func validateAndParseCommandLineParameters(application *kingpin.Application) error {

	if types.App.Settings.Year == 0 {
		if fileNameRegExp.MatchString(filepath.Base(types.App.Settings.Filename)) {
			matches := fileNameRegExp.FindStringSubmatch(filepath.Base(types.App.Settings.Filename))
			types.App.Settings.Year, _ = strconv.Atoi(matches[1])
			logging.Trace.Printf("Parsed year '%v' from filename", types.App.Settings.Year)
			logging.Trace.Printf("Ignoring filename month for now")
		} else {
			return errors.New("Year not provided, and filename does not contain year and month: " + types.App.Settings.Filename)
		}
	}
	return nil
}

func initializeLogging() {
	traceTarget := ioutil.Discard
	infoTarget := ioutil.Discard
	if types.App.Settings.Verbose {
		traceTarget = os.Stdout
		infoTarget = os.Stdout
	}

	warnTarget := os.Stdout
	errorTarget := os.Stderr

	logging.InitLog(traceTarget, infoTarget, warnTarget, errorTarget)
}

func main() {
	types.App.Cli = types.App.Cli.Version("0.0.5")
	types.App.Cli = types.App.Cli.Author("Gandalf IX")
	types.App.Cli = types.App.Cli.Validate(validateAndParseCommandLineParameters)
	configureCommandLineParameters()

	initializeLogging()

	_, error := types.App.Cli.Parse(os.Args[1:])

	initializeLogging()

	types.App.Cli.FatalIfError(error, "")

	parser := &parser.Parser{
		Settings: *types.App.Settings,
	}
	success, error := parser.Parse()
	if !success {
		logging.Error.Println("Error while parsing file: ", error)
	} else {

		validator := &validators.Validator{
			Settings:  *types.App.Settings,
			Timesheet: parser.Timesheet,
		}
		success, errors := validator.Validate()
		if !success {
			logging.Error.Println("Error while validating timesheet: ", joinErrors(errors))
		} else {
			if len(errors) > 0 {
				logging.Warning.Println("Warnings while validating timesheet: ", joinErrors(errors))
			}
			visualizer := &visualizers.Visualizer{
				Settings:  *types.App.Settings,
				Timesheet: parser.Timesheet,
			}
			visualizer.Visualize()
		}
	}
}

func joinErrors(errors []error) string {
	var errorString string
	for _, e := range errors {
		errorString += e.Error() + "\n"
	}
	return errorString
}
