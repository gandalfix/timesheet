package visualizers

import (
	"sort"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

// Visualizer for timesheets
type Visualizer struct {
	Settings  types.TimesheetSettings
	Timesheet types.Timesheet
}

// TimesheetVisualizer visualizes a timesheet
type TimesheetVisualizer struct {
	// Name of this timesheet visualizer
	Name string
	// Description of this visualizer
	Description string
	// Visualize the provided timesheet
	Visualize func(timesheet types.Timesheet)
}

var visualizerRegistry []*TimesheetVisualizer
var visualizerNames []string

// RegisterVisualizer registers the provided validator with the registry for validation
func RegisterVisualizer(visualizer *TimesheetVisualizer) {
	visualizerRegistry = append(visualizerRegistry, visualizer)
	visualizerNames = append(visualizerNames, visualizer.Name)
	sort.Strings(visualizerNames)
}

// Visualize a timesheet with all enabled TimesheetVisualizers
func (visualizer *Visualizer) Visualize() {
	for _, v := range visualizerRegistry {
		if visualizer.Settings.VisualizerName == v.Name {
			logging.Info.Printf("Executing validator '%s'", v.Name)
			v.Visualize(visualizer.Timesheet)
		}
	}
}

// ExistsVisualizer checks whether there is a visualizer registered with the given name
func ExistsVisualizer(name string) bool {
	for _, v := range visualizerRegistry {
		logging.Trace.Printf("Testing validator '%s'", v.Name)
		if v.Name == name {
			return true
		}
	}
	return false
}

// GetVisualizerNames returns the names of all registered visualizers
func GetVisualizerNames() []string {
	names := make([]string, len(visualizerRegistry))
	i := 0
	for _, vis := range visualizerRegistry {
		names[i] = vis.Name
		i++
	}
	sort.Strings(names)
	return names
}
