package validators

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

func init() {
	RegisterValidator(&TimesheetValidator{
		Name:        "TenHours",
		Description: "Validates whether the German 10 hours limit is broken or not",
		Fatal:       false,
		Validate:    ValidateTenHours,
		Priority:    9020,
	})
}

// ValidateTenHours validates whether there is in total more than 10 hours of work
func ValidateTenHours(timesheet types.Timesheet) (ok bool, validationErrors []error) {
	for day, entry := range timesheet.DayEntries {
		logging.Info.Printf("%s", day.Format("02.01.2006"))
		_, localErrors := validateDurationLimit(entry, 10, timesheet)
		if len(localErrors) > 0 {
			validationErrors = append(validationErrors, localErrors...)
		}
	}
	return true, validationErrors
}

func validateDurationLimit(dayEntry *types.DayEntry, limit int, timesheet types.Timesheet) (ok bool, validationErrors []error) {
	var duration time.Duration
	for _, timeEntry := range dayEntry.Times {
		logging.Trace.Printf("Entry  : %s - %s", timeEntry.StartTime.Format("2006-01-02 15:04"), timeEntry.EndTime.Format("15:04"))
		project := timesheet.Projects[timeEntry.ProjectCode]
		if project.ProjectOption == "--ignoreProject" {
			continue
		}
		entryDuration := timeEntry.EndTime.Sub(timeEntry.StartTime)
		duration += entryDuration
	}

	if duration.Hours() > float64(limit) {
		message := fmt.Sprintf("WARNING: You should not work longer than %v hours per day (%s)!", limit, dayEntry.Day.Format("02.01.2006"))
		return true, []error{errors.New(message)}
	}

	return true, []error{}
}
