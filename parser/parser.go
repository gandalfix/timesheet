package parser

import (
	"bufio"
	"errors"
	"os"
	"regexp"
	"strconv"

	"bitbucket.org/gandalfix/timesheet/logging"
	"bitbucket.org/gandalfix/timesheet/types"
)

// Parser is for reading a timesheets file into a timesheet data structure
type Parser struct {
	Timesheet types.Timesheet
	Settings  types.TimesheetSettings
}

type lineType int

const (
	emptyLine lineType = iota
	lineCommentLine
	projectDefinitionLine
	dayDefinitionLine
	timeEntryLine
	unknownLine
)

type parsingState int

const (
	initial parsingState = iota
	inProjectDefintion
	inDay
)

var lineCommentRegexp = regexp.MustCompile(`^#.*`)

// TNG1 TNG-INTERN OfficeTime
// TNG2 TNG-INTERN Fahrtzeit
// KundenProj P001007-002
// KundenProj2 P001007-003 --hideDescription
// PAUSE XXX --ignoreProject
// Unterstunden Timeoff Unterstunden_(optional)
// GanzFrei Timeoff Unterstundentag
var projectDefinitionRegexp = regexp.MustCompile(
	`^(\w+)` + // Code
		` +([0-9A-Za-z_-]+)` + // Project
		//		`(` +
		`( +(.*))?`) //+ // Sub Project
//`( +(--hideDescription|--ignoreProject))?`) // Option
//	`)?$`

//* Fr. 24.04.
var dayRegexp = regexp.MustCompile(`^\* (Mo|Di|Mi|Do|Fr|Sa|So)\. (\d{1,2}\.\d{1,2}\.)$`)

//12:00 - 13:00 PAUSE Mittagessen mit netten Kollegen
var dayEntryRegExp = regexp.MustCompile(`^(\d{2}:\d{2}) - (\d{2}:\d{2}) (\w+)( (.*))?$`)

// Parse actually reads the file and constructs the data structure
func (parser *Parser) Parse() (success bool, error error) {
	parser.Timesheet = *types.NewTimesheet()

	file, err := os.Open(parser.Settings.Filename)
	if err != nil {
		return false, errors.New("Error while opening file: " + err.Error())
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lineNumber := 0

	status := initial

	var currentDay *types.DayEntry

	for scanner.Scan() {
		lineNumber++
		line := scanner.Text()
		logging.Trace.Println(line)

		identifiedLineType := identifyLineType(line)
		switch identifiedLineType {
		case unknownLine:
			logging.Error.Println("Unknown line type found in line ", lineNumber)
			return false, errors.New("Unknown line type found: '" + line + "'")
		case lineCommentLine:
			logging.Trace.Println("LINE_COMMENT")
			// Do nothing else
		case emptyLine:
			// Do nothing
		case projectDefinitionLine:
			logging.Trace.Println("PROJECT_DEFINITION")
			if status == initial || status == inProjectDefintion {
				status = inProjectDefintion
				success, error := handleProjectDefinition(line, &parser.Timesheet)
				if !success {
					return success, error
				}
			} else {
				return false, errors.New("Found PROJECT_DEFINITION line at unexpected position: " + strconv.Itoa(lineNumber))
			}
		case dayDefinitionLine:
			logging.Trace.Println("DAY_DEFINITION")
			if status == initial || status == inProjectDefintion || status == inDay {
				status = inDay
				currentDay = &types.DayEntry{}
				success, error := handleDayEntryDefinition(line, currentDay, parser.Settings.Year)
				parser.Timesheet.DayEntries[currentDay.Day] = currentDay
				if !success {
					return success, error
				}
			}
		case timeEntryLine:
			logging.Trace.Println("DAY_ENTRY")
			if status == inDay {
				success, error := handleTimeEntryDefinition(line, currentDay)
				if !success {
					return success, error
				}
			} else {
				return false, errors.New("Found DAY_ENTRY line at unexpected position: " + strconv.Itoa(lineNumber))
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return false, errors.New("Error while reading file: " + err.Error())
	}

	return true, nil
}
