package parser

import (
	"io/ioutil"
	"os"
	"testing"

	"bitbucket.org/gandalfix/timesheet/logging"
)

func init() {
	logging.InitLog(ioutil.Discard, ioutil.Discard, os.Stderr, os.Stderr)
}

func TestDayParsingOk(t *testing.T) {
	lines := []string{
		"* Mo. 1.4.",
		"* Mo. 01.04.",
		"* Di. 99.99.",
		"* Mi. 24.12.",
		"* Do. 24.04.",
		"* Fr. 24.04.",
		"* Sa. 24.04.",
		"* So. 24.04.",
	}

	for _, line := range lines {
		if !dayRegexp.MatchString(line) {
			t.Error("'", line, "' should be a day definition but does not match")
		}
	}
}

func TestDayEntryParsingOk(t *testing.T) {
	lines := []string{
		"08:00 - 16:00 GanzFrei",
		"08:00 - 10:20 PRJ1 Interne Aufgaben erledigt",
		"10:40 - 10:50 PRJ2 Fahrt Haus - Kunde",
		"10:50 - 12:00 KundenProj Ganz normale Projektarbeit",
		"12:00 - 13:00 PAUSE Mittagessen mit netten Kollegen",
		"13:00 - 17:00 KundenProj2",
		"08:00 - 16:00 Urlaub Freizeit muss auch mal sein",
		"09:00 - 17:00 Krank",
		"08:00 - 10:45 PRJ1 Officearbeit",
		"10:45 - 10:55 PRJ2 Fahrtzeit zum Kunden",
		"10:55 - 18:00 KundenProj Ohne Pause durchgearbeitet",
	}

	for _, line := range lines {
		if !dayEntryRegExp.MatchString(line) {
			t.Error("'", line, "' should be a day definition but does not match")
		}
	}
}

func TestProjectDefinitionOk(t *testing.T) {
	lines := []string{
		"PRJ1 PRJ-INTERN OfficeTime",
		"PRJ2 PRJ-INTERN Fahrtzeit",
		"KundenProj P99105457-002",
		"KundenProj2 PAA1657-003 Working --hideDescription",
		"PAUSE XXX --ignoreProject",
		"Unterstunden Timeoff Unterstunden_(optional)",
		"GanzFrei Timeoff Unterstundentag",
		"Lunch P001000-001 XX --ignoreProject",
		"KundenProj P99105457-002 Vorname Nachname",
		"KundenProj P99105457-002 V. Nachname",
	}

	for _, line := range lines {
		if !projectDefinitionRegexp.MatchString(line) {
			t.Error("'", line, "' should be a project definition but does not match")
		}
	}
}

func TestLineCommentRegExpOk(t *testing.T) {
	lines := []string{"#lineComment", "# LineComment"}

	for _, line := range lines {
		if !lineCommentRegexp.MatchString(line) {
			t.Error("'", line, "' should be a line comment but does not match")
		}
	}
}

func TestLineCommentRegExpFail(t *testing.T) {
	lines := []string{"*10:00 - 16:00 PRJ bla #lineComment", "PRJ # LineComment"}

	for _, line := range lines {
		if lineCommentRegexp.MatchString(line) {
			t.Error("'", line, "' should not be a line comment but does not match")
		}
	}
}
